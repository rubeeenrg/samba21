# SAMBA server
## Rubén Rodríguez ASIX M06-ASO 2021-2022

Imatges docker al DockerHub de [edtasixm06](https://hub.docker.com/u/edtasixm06/)

Documentació del mòdul a [ASIX-M06](https://sites.google.com/site/asixm06edt/)

ASIX M06-ASO Escola del treball de barcelona

### SAMBA Container:

 * **rubeeenrg/samba21:aws** 

#### Documentació:
Primer de tot, fem 'Launch instances' i escollim 't2.small', seguidament, escollim 'Debian 10'.

A continuació anem configurant les diferents opcions de la instància... i a la part de 'Configure Security Group' possem les següents regles:

```
SSH		TCP	22	0.0.0.0/0
Custom TCP	TCP	2200	0.0.0.0/0
SMB		TCP	445	0.0.0.0/0
Custom TCP	TCP	139	0.0.0.0/0
LDAP		TCP	389	0.0.0.0/0
```

Fem 'Next' fins que ens demani la clau SSH i agafem 'vockey | RSA' (clau 'AWS academy')

Anem a 'AWS academy' i ens descarreguem la clau SSH, a continuació, li donem permisos 'chmod 400' a la nostra clau.

Seguidament ens connectem a la nostra màquina d'AWS (previament hem de copiar la IP pública):

```
ssh -i ~/.ssh/<clau> admin@IP
```

### Configuració client: (no és necessari ja que ho fem amb container PAM)
Instal·lem els següents paquets:

```
sudo apt install -y libpam-pwquality libpam-mount nfs-common libpam-ldapd libnss-ldapd nslcd nslcd-utils ldap-utils sshfs openssh-server samba-client cifs-utils
```

Configurem els fitxers següents possant-li la mateixa configuració que hi tenim dins del directori 'pam21:ldap'

```
sudo vim /etc/ldap/ldap.conf
sudo vim /etc/nsswitch.conf
sudo vim /etc/nslcd.conf
```

Configurem el 'pam_mount.conf.xml' i li possem a 'servidor' la IP d'Amazon:

```
sudo vim /etc/security/pam_mount.conf.xml
```

Fiquem la IP pública d'Amazon dins de '/etc/hosts' per poder entrar amb el domini (en cas de voler fer proves o alguna cosa):

```
sudo vim /etc/hosts
```

#### Configuració servidor:
Un cop dins de la nostra instància, instal·lem 'docker' i 'docker compose':

DOCKER:

```
https://docs.docker.com/engine/install/debian
```

DOCKER COMPOSE:

```
https://docs.docker.com/compose/install
```

Un cop instal·lats, copiem el nostre 'docker-compose.yml' a la màquina d'AWS (si dona error, fem un cat i el copiem a mà):

```
scp -i ~/.ssh/<clau> <path_to_docker-compose.yml> admin@IP:/<path_destí>
```

Creem la nostra 'network': (no cal)

```
sudo docker network create 2hisix
```

Inicialitzem el 'docker-compose.yml':

```
sudo docker-compose up -d
```

Comprobem que tot està funcionant correctament:

```
sudo docker ps
```

### Configuració container PAM:.
Entrem al container:

```
docker run --rm --name pam.edt.org -h pam.edt.prg --net 2hisix --privileged -it rubeeenrg/pam21:ldap /bin/bash
```

Possem dins del 'etc/hosts' la IP d'AWS amb ldap i smb:

```
<IP>	ldap.edt.org ldap
<IP>	smb.edt.org smb
```

Executem el 'startup':

```
bash startup
```

Comprovem que s'ha generat tot correctament:

```
getent passwd
```

Anem al 'pam_mount.conf.xml' i posem la IP de l'AWS a la línea de 'server':

```
vim /etc/security/pam_mount.conf.xml
```

Entrem com a un usuari samba / ldap i comprovem que està tot correcte:

```
su - anna
pwd
ls -la
```

### Inicialització:
DOCKER (només si volem entrar per alguna cosa):

``` 
sudo docker exec -it ldap.edt.org /bin/bash
sudo docker exec -it smb.edt.org /bin/bash
```

DOCKER COMPOSE:

```
docker-compose up -d
docker-compose down
```
